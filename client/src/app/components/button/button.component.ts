import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

export enum ButtonType{
  Simple,
  Primary,
}
@Component({
    selector: 'app-button',
    standalone: true,
    imports: [
        CommonModule,
    ],
    template: `<button type="button" class='button-simple'>{{text}}</button>`,
    styleUrl: './button.component.css',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
  @Input() text:string='Новая кнопка';
  @Input() type:ButtonType=ButtonType.Simple;

}
