
import { Component} from '@angular/core';
import { MainComponent } from './pages/main/main.component';



@Component({
  selector: 'app-root',
  template: '<app-main></app-main>',
  standalone: true,
  imports: [MainComponent],
})
export class AppComponent  {

}
