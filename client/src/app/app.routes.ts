import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./pages/home/home.component').then((c) => c.HomeComponent),
  },
  {
    path: 'products',
    loadComponent: () => import('./pages/products/products.component').then((c) => c.ProductsComponent),
  },
  {
    path: 'services',
    loadComponent: () => import('./pages/services/services.component').then((c) => c.ServicesComponent),
  },
  {
    path: 'questions',
    loadComponent: () => import('./pages/questions/questions.component').then((c) => c.QuestionsComponent),
  },
  {
    path: 'contacts',
    loadComponent: () => import('./pages/questions/questions.component').then((c) => c.QuestionsComponent),
  },
  { path: '**', redirectTo: '' },
];
