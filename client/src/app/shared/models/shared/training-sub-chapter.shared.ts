export type TrainingSubChapterResReq = {
  id?: number;
  name: string;
  sorting?: number;
};
