import { JlptReq } from './constant.shared';
import { LocalFileShared } from './local-files.shared';

export type ReadingResReq = {
  id?: number;
  create_at?: Date;
  update_at?: Date;
  jlpt?: JlptReq;
  jlptId?: number;
  image?: LocalFileShared;
  imageId?: number;
  title: string;
  description: string;
};
