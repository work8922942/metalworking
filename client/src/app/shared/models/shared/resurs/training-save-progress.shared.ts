import { TrainingPassType } from './training-pass-type.shared';

export type TrainingSaveProgressRequest = {
  trainingId: number;
  type: TrainingPassType;
  progress: { question: string; answer: string; isCorrect: boolean }[];
};
