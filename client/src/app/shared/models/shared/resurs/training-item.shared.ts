export type TrainingItem = {
  id: number;
  name: string;
  pass?: number;
  sorting?: number;
  count: number;
  difficult: number;
  chapter: TrainingChapter;
  sub_chapter: TrainingChapter;
};

export type TrainingChapter = {
  id: number;
  name: string;
};
