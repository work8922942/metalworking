export type TrainingPassingType = {
  symbol: string;
  value: string;
  progress: number;
  furigana?: string;
};
