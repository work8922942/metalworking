import { TrainingItem } from './training-item.shared';

export type TrainingListRequest = {
  pageIndex: number;
  pageSize: number;
  where?: {
    search?: string;
    chapterId?: number;
    subChapterId?: number;
  };
};
export type TrainingListResponse = {
  count: number;
  nodes: TrainingItem[];
};
