export type ChapterFilter = {
  id: number;
  name: string;
  count: number;
};
