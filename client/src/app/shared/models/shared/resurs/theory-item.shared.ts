import { JlptReq } from '../constant.shared';

export type TheoryItem = {
  id: number;
  create_at: Date;
  title: string;
  short_description: string;
  jlpt: JlptReq;
  text?: string;
  raiting?: number;
};
