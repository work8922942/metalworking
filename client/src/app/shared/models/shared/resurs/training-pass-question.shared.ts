export type TrainingPassQuestion = {
  question: string;
  furigana?: string;
  kun_readings?: string[];
  on_readings?: string[];
  answers: string[];
  correct: string;
};
