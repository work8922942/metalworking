export type TheoryCategory = {
  id: number;
  name: string;
  theory_count: number;
};
