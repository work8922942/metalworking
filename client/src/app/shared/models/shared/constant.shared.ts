export type JlptReq = {
  id: number;
  name: string;
};
export type WanikaniReq = {
  id: number;
  name: string;
};
export type JoyoReq = {
  id: number;
  name: string;
};
