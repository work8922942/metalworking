import { KanjiReqRes } from './kanji.shared';
import { TrainingChapterResReq } from './training-chapter.shared';
import { TrainingSubChapterResReq } from './training-sub-chapter.shared';
import { WordResReq } from './word.shared';

export enum TrainingTypeQuestion {
  KANJI = 'kanji',
  WORD = 'word',
}

export const TrainingTypeQuestionName: Record<TrainingTypeQuestion, string> = {
  kanji: 'Словарь кандзи',
  word: 'Словарь слов',
};

export type TrainingResReq = {
  id?: number;
  name: string;
  count: number;
  difficult: number;
  sorting?: number;
  chapter: TrainingChapterResReq;
  sub_chapter: TrainingSubChapterResReq;
  type_questions: TrainingTypeQuestion;
  list_words: WordResReq[];
  list_kanji: KanjiReqRes[];
};
