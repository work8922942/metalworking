export type TrainingChapterResReq = {
  id?: number;
  name: string;
  sorting?: number;
};
