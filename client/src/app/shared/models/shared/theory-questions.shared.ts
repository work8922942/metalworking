export type TheoryQuestions = {
  id?: number;
  question: string;
  answers: string[];
  correct: string;
};
