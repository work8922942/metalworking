import { KanjiReqRes } from './kanji.shared';
import { WordTypeResReq } from './word-type.shared';

export type WordResReq = {
  id?: number;
  furigana?: string;
  word: string;
  values: string[];
  word_type: WordTypeResReq[];
  list_kanji: KanjiReqRes[];
  description: string;
  count_in_test?: number;
};
