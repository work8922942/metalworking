import { JlptReq } from './constant.shared';
import { TheoryQuestions } from './theory-questions.shared';

export type TheoryResReq = {
  id?: number;
  create_at: Date;
  title: string;
  short_description: string;
  jlpt: JlptReq;
  jlptId?: number;
  text: string;

  questions?: TheoryQuestions[];
};
