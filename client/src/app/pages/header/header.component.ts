import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ButtonComponent } from '../../components/button/button.component';
import { RouterLink } from '@angular/router';
@Component({
    selector: 'app-header',
    standalone: true,
    imports: [
        CommonModule,ButtonComponent,RouterLink
    ],
    template: `
      <div class="header-block">
        <a class="logo" [routerLink]="['/home']">
          <!-- <img src="assets/images/logo.png" alt="logo"> -->
          Лого
        </a>
        <nav class="nav">
          <ul>
            <li [routerLink]="['/products']">Продукция</li>
            <li [routerLink]="['/services']">Услуги</li>
            <li [routerLink]="['/questions']">Вопросы</li>
            <li [routerLink]="['/contacts']"> Контакты</li>
          </ul>
        </nav>
      </div>
      <div class="header-block">
        <div class="header-tel">
        <a href="tel:+375336666666">+375(33) 787 34 22</a>
        </div>
        <div class="header-button">
          <app-button text='Заказать звонок'></app-button>
        </div>
      </div>
    `,
    styleUrl: './header.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent { }
