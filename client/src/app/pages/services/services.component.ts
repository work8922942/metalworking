import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-services',
    standalone: true,
    imports: [
        CommonModule,
    ],
    template: `<p>services works!</p>`,
    styleUrl: './services.component.css',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicesComponent { }
