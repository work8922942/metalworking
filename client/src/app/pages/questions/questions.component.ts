import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-questions',
    standalone: true,
    imports: [
        CommonModule,
    ],
    template: `<p>questions works!</p>`,
    styleUrl: './questions.component.less',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionsComponent { }
